var express = require("express");
var bodyParser = require("body-parser");
var path = require("path");
var expressValidator = require("express-validator");
var mongojs = require("mongojs");

// Adds database credentials
var db = mongojs(require("./config/database"));
var ObjectId = mongojs.ObjectId;

var app = express();

// View Engine
app.set("view engine", "ejs");
app.set("views", path.join(__dirname, "views"));

// Body Parser Middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// Set Static Path
app.use(express.static(path.join(__dirname, "public")));

// Global Variables
app.use(function(req, res, next) {
  res.locals.errors = null;
  next();
});

// Express Validator Middleware
app.use(expressValidator());

app.get("/", function(req, res) {
  db.users.find(function(err, docs) {
    res.render("index", {
      title: "Welcome to our Page",
      users: docs
    });
  });
});

app.post("/users/add", function(req, res) {
  req.checkBody("first_name", "First Name is required").notEmpty();
  req.checkBody("last_name", "Last Name is required").notEmpty();
  req.checkBody("email", "Email Address is required").notEmpty();

  var errors = req.validationErrors();

  if (errors) {
    res.render("index", {
      title: "Welcome to myeApp",
      users: users,
      errors: errors
    });
  } else {
    var newUser = {
      first_name: req.body.first_name,
      last_name: req.body.last_name,
      email: req.body.email
    };

    db.users.insert(newUser, function(err, result) {
      if (err) {
        console.log(err);
      }
      res.redirect("/");
    });
  }
});

app.delete("/users/delete/:id", function(req, res) {
  db.users.remove({ _id: ObjectId(req.params.id) }, function(err, result) {
    if (err) {
      console.log(err);
    }
    res.redirect("/");
  });
});

app.set("port", process.env.PORT || 3001);

app.listen(app.get("port"), function() {
  console.log("Server started on port " + app.get("port"));
});
