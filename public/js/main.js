$(document).ready(function() {
  $(".deleteUser").on("click", deleteUser);
});

function deleteUser() {
  var confirmation = confirm("Deletion can not be reversed. Are you sure?");

  if (confirmation) {
    $.ajax({
      type: "DELETE",
      url: "/users/delete/" + $(this).data("id")
    }).done(function(response) {
      window.location.replace("/");
    });
  } else {
    return false;
  }
}

var canvas = document.getElementById("myCanvas");
var c = canvas.getContext("2d");
console.log(canvas);

canvas.width = window.innerWidth;
canvas.height = window.innerHeight;

// Draws a rectangle
// c.fillStyle = "rgba(255,0,0,0.1)";
// c.fillRect(100, 100, 100, 100);
// c.fillStyle = "rgba(0,255,0,0.1)";
// c.fillRect(150, 150, 200, 200);
// c.fillStyle = "rgba(0,0,255,0.1)";
// c.fillRect(300, 100, 100, 100);

// Draws a line
// c.beginPath();
// c.moveTo(50, 300);
// c.lineTo(350, 400);
// c.strokeStyle = "#fa34a3";
// c.stroke();

// // Draws another line
// c.beginPath();
// c.moveTo(60, 310);
// c.lineTo(360, 410);
// c.stroke();

// Draws an Arc / Circle
// c.beginPath();
// c.arc(300, 300, 30, 0, Math.PI * 2, false);
// c.strokeStyle = "#444";
// c.stroke();

// Draws Circles within for loop
for (var i = 0; i < 80; i++) {
  var x = Math.random() * window.innerWidth;
  var y = Math.random() * window.innerHeight;
  c.beginPath();
  c.arc(x, y, 30, 0, Math.PI * 2, false);
  c.strokeStyle = "#ccc";
  c.stroke();
}

// ANIMATES CIRCLE
var x = 100;
var y = 100;

var radius = 30;
var dx = (dy = 8);

function animate() {
  requestAnimationFrame(animate);
  c.clearRect(0, 0, innerWidth, innerHeight);

  c.beginPath();
  c.arc(x, y, 30, 0, Math.PI * 2, false);
  c.strokeStyle = "#ccc";
  c.stroke();

  if (x - radius > innerWidth - x || x - radius < 0) {
    dx = -dx;
  }

  x += dx;
}

animate();

// ANIMATES TEXT
// var x = 200;
// var y = 200;
// var dx = (dy = 4);

// function animate() {
//   requestAnimationFrame(animate);
//   c.font ="30px Arial";

//   c.beginPath();
//   c.arc(x, y, 30, 0, Math.PI * 2, false);
//   c.strokeStyle = "#ccc";
//   c.stroke();

//   if (x + radius > innerWidth || x - radius < 0) {
//     dx = -dx;
//   }

//   if (y + radius > innerHeight || y - radius < 0) {
//     dy = -dy;
//   }

//   x += dx;
//   y += dx;
// }

// animate();
